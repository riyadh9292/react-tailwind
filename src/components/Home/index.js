import React, { useEffect, useState } from "react";
import Content from "../Content";
import DropDown from "../DropDown";
import Footer from "../Footer";
import Hero from "../Hero";
import Navbar from "../Navbar";

const Home = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  };
  useEffect(() => {
    const hideMenu = () => {
      if (window.innerWidth > 768 && isOpen) {
        setIsOpen(false);
        console.log("working");
      }
    };
    window.addEventListener("resize", hideMenu);
    return () => {
      window.removeEventListener("resize", hideMenu);
    };
  }, []);

  return (
    <div>
      <Navbar toggle={toggle} />
      <DropDown isOpen={isOpen} toggle={toggle} />
      <Hero />
      <Content />
      <Footer />
    </div>
  );
};

export default Home;
